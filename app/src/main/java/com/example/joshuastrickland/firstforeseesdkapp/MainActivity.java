package com.example.joshuastrickland.firstforeseesdkapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.foresee.sdk.ForeSee;

/*
In the manifest file:
android:name - The fully qualified name of an Application subclass implemented for the application. When the application process is started, this class is instantiated before any of the application's components.
The subclass is optional; most applications won't need one. In the absence of a subclass, Android uses an instance of the base Application class.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void launchInvite(View v){
        //ForeSee.showInviteForSurveyID("app_test_1");
        ForeSee.checkIfEligibleForSurvey();
    }

    public void resetState(View v){
        ForeSee.resetState();
    }
}